// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  branding: "TipProject Local",
  cognito: {
    endpoint: 'https://tipproject-customer.auth.us-east-1.amazoncognito.com',
    userClientId: '1mpamc33gk7tp9l2gb6caihe3s',
    userSecret: '76s0rcg6sj6gotpuk8eb6psatu9pkoqnkv7mrgnidkn62ttpi8c',
    userRedirectEndpoint: 'http://localhost:4200',
    region: 'us-east-1',
    accessKeyId : 'ASIAY6BDDYN4O5QHHQ7N',
    secretAccessKey  : 'msCW8Eghsj+KcNyzQFGP8OlChK6UvjQ6CSH/MAlC',
    sessionToken  : 'FwoGZXIvYXdzEJf//////////wEaDK74zbTzoOOSnCeSUCLPARaoAXpMV273DfzvKG1x3Q5QkXBTjAvrXFsQ+Tr8i3z6O8h4cxmGUvrMl8/TUxd/GCapgVoYSVE29rd8Sj5JVd746znRt42caZeKZF7r3qksscSi+Dmznj9CUaEn+uzgMXk7veGg2VWhZn2eluI12Zdyr4Qgn+71mB4dD+3jnmJNo7KQmBn4vTBePi6VOsu6//jCM9mhFH9fcv0pudMuRVbo8hZ7JCiW3IQndr7hA5+9OfSp3Z2/YER/npZXbBcku1794g8s9ACYNWnbqlRKJSiNmtD1BTIt3LaEdeUpyyAnWmtfjRn9IEM2C1AzjFg08WeuC6GhXyaH0etz7spa7G4gxJw++KcNyzQFGP8OlChK6UvjQ6CSH/MAlC',
  },
  gateway: {
    endpoint: 'https://elo9vkp2l8.execute-api.us-east-1.amazonaws.com/dev',
  },
  sessionStorageKeys: {
    idToken: 'idToken',
    accessToken: 'accessToken',
    refreshToken: 'refreshAccessToken',
    user: 'loggedInUser',
  },
};
