export const environment = {
  production: true,
  branding: "TipProject",
  cognito: {
    endpoint: '',
    userClientId: '',
    userSecret: '',
    userRedirectEndpoint: '',
    region: '',
    accessKeyId : '',
    secretAccessKey  : '',
    sessionToken  : '',
  },
  gateway: {
    endpoint: 'https://elo9vkp2l8.execute-api.us-east-1.amazonaws.com',
  },
  sessionStorageKeys: {
    idToken: 'idToken',
    accessToken: 'accessToken',
    refreshToken: 'refreshAccessToken',
    user: 'loggedInUser',
  },
};
