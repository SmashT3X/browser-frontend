import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {WelcomeComponent} from "./welcome/welcome.component";
import {LoginComponent} from "./login/login.component";
import {UserAuthenticationGuard} from "./guard/user-authentication.guard";
import {UserDashboardComponent} from "./dashboards/user-dashboard/user-dashboard.component";
import {AuthorizationGuard} from "./util/authorization.guard";
import {LoginUserResultComponent} from "./login/login-user-result/login-user-result.component";


const routes: Routes = [
  {path: '', component: WelcomeComponent},
  {path: 'login', component: LoginComponent},
  {path: 'login-user-result', component: LoginUserResultComponent, canActivate: [UserAuthenticationGuard]},
  {path: 'user-dashboard', component: UserDashboardComponent, canActivate: [AuthorizationGuard]},];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
