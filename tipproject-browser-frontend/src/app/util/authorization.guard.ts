import { Injectable } from '@angular/core';
import {CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot, UrlTree, ParamMap, Router} from '@angular/router';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class AuthorizationGuard implements CanActivate {

  sessionAccessToken: string;

  constructor(private router: Router) {
  }

  canActivate(
    next: ActivatedRouteSnapshot,
    state: RouterStateSnapshot): Observable<boolean | UrlTree> | Promise<boolean | UrlTree> | boolean | UrlTree {
    this.sessionAccessToken = sessionStorage.getItem('accessToken');
    // console.log('SESSION ACCES TOKEN: ' + this.sessionAccessToken);
    if (this.sessionAccessToken != null && this.sessionAccessToken.length > 0) {
      return true;
    }
    this.router.navigate([''])
    return false;
  }

}
