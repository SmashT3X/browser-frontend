import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { LoginUserResultComponent } from './login-user-result.component';

describe('LoginUserResultComponent', () => {
  let component: LoginUserResultComponent;
  let fixture: ComponentFixture<LoginUserResultComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ LoginUserResultComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LoginUserResultComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
