import {Injectable} from '@angular/core';
import {
  ActivatedRoute,
  ActivatedRouteSnapshot,
  CanActivate,
  ParamMap,
  Router,
  RouterStateSnapshot,
  UrlTree
} from '@angular/router';
import {HttpClient, HttpHeaders} from "@angular/common/http";
import {Observable} from "rxjs";
import {TokenRequest} from "../model/http/token-request";
import {environment} from "../../environments/environment";
import {TokenResponse} from "../model/http/token-response";
import {User} from "../model/user";

@Injectable({
  providedIn: 'root'
})
export class UserAuthenticationGuard implements CanActivate {

  sessionAccessToken: string;
  accessToken: string;
  tokenUrl: string = environment.cognito.endpoint + '/oauth2/token';
  userInfoUrl: string = environment.cognito.endpoint + '/oauth2/userInfo';

  constructor(private route: ActivatedRoute,
              private router: Router,
              private http: HttpClient) {
  }


  canActivate(
    next: ActivatedRouteSnapshot,
    state: RouterStateSnapshot): Observable<boolean | UrlTree> | Promise<boolean | UrlTree> | boolean | UrlTree {
    return this.handleActivation();
  }

  handleActivation(): boolean {
    this.sessionAccessToken = sessionStorage.getItem('accessToken');
    if (this.sessionAccessToken == null || this.sessionAccessToken.length == 0) {
      this.route.queryParamMap.subscribe((paramMap: ParamMap) => {
        this.handleCode(paramMap.get('code'));
      })
      return true;
    }
    return false;
  }

  handleCode(authCode: string) {
    let tokenRequest: TokenRequest = new TokenRequest();
    tokenRequest.code = authCode;
    if (tokenRequest.valid()) {
      let authValue: string = btoa(tokenRequest.client_id + ":" + environment.cognito.userSecret);
      let auth: string = 'Basic ' + authValue;
      // console.log('Token Auth: ' + auth);
      let headers: HttpHeaders = new HttpHeaders({
        'Content-Type': 'application/x-www-form-urlencoded',
        'Authorization': auth,
      })
      // headers.set('Content-Type', 'application/x-www-form-urlencoded')
      // headers.set('Authorization', 'Basic ' + auth);
      let header: string = '';
      headers.keys().forEach(value => {
        header = header + value + ':' + headers.get(value) + '\n'
      })
      // console.log('Token Header Keys: ' + header);
      const body: URLSearchParams = new URLSearchParams();
      body.append('grant_type', tokenRequest.grant_type)
      body.append('client_id', tokenRequest.client_id)
      body.append('redirect_uri', tokenRequest.redirect_uri)
      body.append('code', tokenRequest.code)
      // console.log('Token Body: ' + body.toString());
      this.http.post(this.tokenUrl, body.toString(), {
        headers: headers,
        reportProgress: true,
        withCredentials: true,
        observe: 'body',
        responseType: 'json',
      }).subscribe(
        response => {
          console.log("Token response: " + JSON.stringify(response))
          let tokenResponse: TokenResponse = JSON.parse(JSON.stringify(response))
          // console.log("Token response OBJECT access_token: " + tokenResponse.access_token)
          sessionStorage.setItem(environment.sessionStorageKeys.idToken, tokenResponse.id_token);
          sessionStorage.setItem(environment.sessionStorageKeys.accessToken, tokenResponse.access_token);
          sessionStorage.setItem(environment.sessionStorageKeys.refreshToken, tokenResponse.refresh_token);
          this.getCognitoUserInfo(tokenResponse.access_token);
        },
        error => console.log("Token response ERROR: " + JSON.stringify(error)),
      );
    }
  }

  getCognitoUserInfo(accessToken: string) {
    let getUserUrl = environment.gateway.endpoint + '/frontend-api/getUser';
    let headers: HttpHeaders = new HttpHeaders({
      'Authorization': accessToken,
    });
    this.http.get(getUserUrl, {
      headers: headers,
      reportProgress: true,
      withCredentials: true,
      observe: 'body',
      responseType: 'json',
    }).subscribe(response => {
        // console.log("UserInfo response: " + JSON.stringify(response))
        let user: User = JSON.parse(JSON.stringify(response));
        // console.log("UserInfo user: " + JSON.stringify(user))
        sessionStorage.setItem(environment.sessionStorageKeys.user, JSON.stringify(user));
        this.router.navigate(['user-dashboard'])
      },
      error => console.log("UserInfo response ERROR: " + JSON.stringify(error)));

  }

}
