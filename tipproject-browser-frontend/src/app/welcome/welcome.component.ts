import {Component, OnInit} from '@angular/core';
import {Router} from "@angular/router";
import {User} from "../model/user";
import {UserService} from "../service/user.service";

@Component({
  selector: 'app-welcome',
  templateUrl: './welcome.component.html',
  styleUrls: ['./welcome.component.scss']
})
export class WelcomeComponent implements OnInit {

  title: string = 'Welcome';
  user: User;

  ngOnInit() {
    this.user = this.userService.getUser();
  }


  constructor(private readonly router: Router,
              private readonly userService: UserService) {
  }

}
