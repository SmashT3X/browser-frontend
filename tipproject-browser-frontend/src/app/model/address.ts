export class Address {

  public street: string;

  public zipCode: string;

  public city: string;

  public country: string;

}
