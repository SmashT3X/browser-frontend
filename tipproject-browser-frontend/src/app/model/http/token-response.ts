export class TokenResponse {

  public access_token: string;
  public refresh_token: string;
  public id_token: string;
  public token_type: string;
  public expires_in: string;

}
