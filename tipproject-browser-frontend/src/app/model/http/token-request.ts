import {environment} from "../../../environments/environment";

export class TokenRequest {

  public grant_type: string = 'authorization_code';
  public client_id: string = environment.cognito.userClientId;
  public redirect_uri: string = environment.cognito.userRedirectEndpoint + '/login-user-result';
  public code: string;

  valid(): boolean {
    return this.code != null && this.code.length > 0;
  }


}
