export class UserInfoResponse {

  sub: string;
  username: string;
  name: string;
  given_name: string;
  family_name: string;
  email: string;

}
