import {Address} from "./address";
import {ContactData} from "./contact-data";

export class User {

  public username: string;

  public email: string;

  public name: string;

  public firstName: string;

  public verifiedForReceiving: boolean;

  public profilePhoto: string;

  public address: Address = new Address();

  public contactData: ContactData = new ContactData();

  constructor() {
  }

  public toString(): string {
    return JSON.stringify(this);
  }

}
