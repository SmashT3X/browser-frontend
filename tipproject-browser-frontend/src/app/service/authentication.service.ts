import {Injectable} from '@angular/core';
import {environment} from "../../environments/environment";
import {Router} from "@angular/router";

@Injectable({
  providedIn: 'root'
})
export class AuthenticationService {

  private scopes: Array<string> = ["email", "openid", "https://tipproject.com/frontend-api/users.get", "aws.cognito.signin.user.admin", "https://tipproject.com/frontend-api/users.attributes.update"];

  constructor(private readonly router: Router) { }

  loginUser() {
    let loginUrl: string;
    let scopeParameter: string = '';
    this.scopes.forEach(value => {
      scopeParameter = scopeParameter + value + "+";
    })
    console.log("SCOPEPARAMETER: " + scopeParameter)
    loginUrl = environment.cognito.endpoint + '/login' +
      '?client_id=' + environment.cognito.userClientId +
      '&response_type=code' +
      '&scope=' + scopeParameter +
      '&redirect_uri=' + environment.cognito.userRedirectEndpoint + '/login-user-result';
    window.location.href = loginUrl;
  }

  loginOrganization() {
  }

  logout() {
    sessionStorage.clear();
    this.router.navigate(['']);
  }

}
