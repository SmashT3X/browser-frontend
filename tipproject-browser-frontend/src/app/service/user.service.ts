import { Injectable } from '@angular/core';
import {BackendService} from "./backend.service";
import {User} from "../model/user";
import {useAnimation} from "@angular/animations";
import {Observable, Subject} from "rxjs";

@Injectable({
  providedIn: 'root'
})
export class UserService {

  constructor(private readonly backendService: BackendService) {

  }

  getUser(): User {
    let result: User = JSON.parse(sessionStorage.getItem("loggedInUser"));
    // console.log("UserService getUser: " + JSON.stringify(result));
    return result;
  }

}
