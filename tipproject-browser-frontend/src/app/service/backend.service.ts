import { Injectable } from '@angular/core';
import {User} from "../model/user";
import {environment} from "../../environments/environment";
import {HttpClient, HttpHeaders} from "@angular/common/http";
import {Observable, Subject} from "rxjs";

@Injectable({
  providedIn: 'root'
})
export class BackendService {

  constructor(private http: HttpClient) { }

  getSessionUser(): Observable<User> {
    let user: Subject<User> = new Subject<User>();
    let gatewayUrl = environment.gateway.endpoint + "/frontend-api/getuser";
    let auth: string = 'Bearer ' + sessionStorage.getItem(environment.sessionStorageKeys.refreshToken);
    let headers: HttpHeaders = new HttpHeaders({
      'Access-Control-Allow-Origin': 'true',
      'Authorization': auth,
    })
    this.http.get(gatewayUrl, {
      headers: headers
    }).subscribe(response => {
      console.log('GetUser Response: ' + JSON.stringify(response));
      user.next(new User())
    }, error => {
      console.log('GetUser Error: ' + JSON.stringify(error));
      user.next(new User())
    })
    return user.asObservable();
  }

}
