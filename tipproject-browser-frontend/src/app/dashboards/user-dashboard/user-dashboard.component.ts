import {Component, OnInit} from '@angular/core';
import {User} from "../../model/user";
import {UserService} from "../../service/user.service";

@Component({
  selector: 'app-user-dashboard',
  templateUrl: './user-dashboard.component.html',
  styleUrls: ['./user-dashboard.component.scss']
})
export class UserDashboardComponent implements OnInit {

  user: User;

  title: string = 'User Dashboard';


  constructor(private userService: UserService) {
  }

  ngOnInit(): void {
    this.user = this.userService.getUser();
    console.log("UserDashboard user: " + JSON.stringify(this.user));
  }

}
