import {Component, Input, OnInit} from '@angular/core';
import {User} from "../model/user";
import {AuthenticationService} from "../service/authentication.service";
import {environment} from "../../environments/environment";
import {Router} from "@angular/router";

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss']
})
export class HeaderComponent implements OnInit {

  panelOpenState = false;

  @Input() user: User;

  branding: string = environment.branding;

  @Input() title: string;

  constructor(private readonly authenticationService: AuthenticationService,
              private readonly router: Router) {
  }

  ngOnInit(): void {
  }

  logout() {
    this.authenticationService.logout();
  }

  showUserDashboard() {
    this.router.navigate(['user-dashboard'])
  }

  showMenu() {
    // this.menuOpenedChange.emit();
  }

  loginUser() {
    this.authenticationService.loginUser();
    // this.router.navigate(['login'])
  }

  loginOrganization() {
    this.authenticationService.loginOrganization();
    // this.router.navigate(['login'])
  }

}
